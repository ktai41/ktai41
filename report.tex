% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt


\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{url}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\usepackage[colorlinks,urlcolor=cyan,citecolor=red]{hyperref}
\usepackage{float}
%%% END Article customizations

%%% The "real" document content comes below...

\title{Generation of Common Sense Knowledge}
\author{Kerkhof, Iddo and Kluft, Ronald}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle
\tableofcontents

\section{Introduction}

This report describes the evaluation of a paper on common sense knowledge graphs for Assignment 4.1 of the Key Topics in AI course. We have evaluated the paper and its associated code. This report starts with a summary, followed by exploratory data analysis, after which we generated test data. The dataset was then split and automatically evaluated. Then we started experimenting with different techniques. The results are compared with the results in the paper.

\section{Summary paper}
This is a summary of the paper \\
\textbf{(COMET)-ATOMIC-2020: On Symbolic and Neural Commonsense Knowledge Graphs} \cite{Hwang2021COMETATOMIC2O}\\

In recent years, there has been a renewed interest in common sense representation and reasoning in the field of natural language understanding. Central to this progress has been the development of new Commonsense Knowledge Graphs (CSKG). Their diverse facts can be used and referenced by machine learning models to tackle new and challenging tasks. However, questions remain about the quality and coverage of these resources. This is due to the sheer scale needed to fully encompass common sense knowledge.
The authors argue that manually constructed CSKGs will never achieve the coverage necessary to be applicable in all situations encountered by Natural Language Processing (NLP) agents. Therefore, a new goal is to establish a new evaluation framework for testing the usefulness of knowledge graphs (KG) based on how effectively implicit knowledge representations can be learned from them. To this end, the authors present ATOMIC-2020, a new CSKG that incorporates knowledge not readily available in pre-trained language models. They compared its properties with other leading CSKGs and conducted the first large-scale pairwise study of common sense sources.
They show that ATOMIC-2020 is better suited for training knowledge models that can generate accurate, representative knowledge for new, unseen entities and events.
Finally, they created COMET-Atomic2020 (COMET stands for COMmonsEnse Transformers)\cite{bosselut2019comet}.   This is a neural knowledge model that combines a deep learning model with a KG. The knowledge of the KG is successfully transferred to the neural network so that it outperforms the state-of-the-art language model GPT-3 with regard to declarative knowledge.
This demonstrates the utility and importance of high-quality symbolic knowledge provided by ATOMIC-2020 to generalize information to common sense that language models cannot expressively capture at their own.


\subsection{Relation to KTAI}

The article by Davis et al \cite{davis2015commonsense} is several years older than article \cite{Hwang2021COMETATOMIC2O}. It gives a rather negative impression on common sense reasoning, while the authors of Atomic-2020 are actually enthusiastic about it. They talk about crowdsourcing, the technique the authors of Atomic-2020 used, and they point out the enormous cost, and whether it is an efficient tool. In defense we would say that any means of filtering and confirming the collected data, no matter how slow or expensive in the beginning, is good. Otherwise, you'll be left with messy data until better solutions come along.

In the article GOFAI\cite{boden20144}, which stands for Good Old-Fashioned AI, Boden argues that the weaknesses and strengths of GOFAI and PDP (parallel distributed processing) connectionism complement each other. GOFAI involves the construction and transformation of symbolic data structures. A knowledge graph is one such structure. Deep learning models, on the other hand, are an example of PDP connectionism. Therefore, COMET-Atomic2020 can be seen as a hybrid system of the two paradigms.
Such a combination has better potential than the separate components. For example, one of the weaknesses of GOFAI is learning new concepts, which is only possible by manually defining their features. Deep learning, on the other hand, excels at automatically learning new features. The weaknesses of PDP connectionism lie in the modeling of, for example, multi-level hierarchy and inferential relationships, which can be well modeled with a KG. 
The combination of KGs and deep learning models is therefore a good example of a hybrid approach that works well, as shown by the results in the article.


\section{Preparation for experimentation}
The purpose of this report is to split the data used in the paper in multiple ways and evaluate each subset separately. By experimenting with different ways of splitting the data, we aim to discover the strengths and weaknesses of the model.
Before conducting these experiments, the data is first analyzed using exploratory data analysis. After that, new test data is generated with the model. Next, the splitting and evaluation of this test data is automated so that more experiments can easily be added if it proves desirable.

\subsection{Exploratory data analysis}\label{EDA}
Before experimenting with the data, we began the process of exploratory data analysis to find out how the data is structured and how it can be broken down into categories. In the code for the paper, the file automatic\_eval.py is used for evaluation. It refers to a test data file BART-atomic\_2020.json with 5000 generated examples consisting of a head, a relation, a tail, multiple generations and a greedy generation.

To explore the data, we split it into several buckets using a bucket function that returns a label for each example. Then a dictionary was created with the label as the key and a list of examples as the value. Finally, for each bucket the key, the number of examples and the percentage of the total were printed. For the results, see the appendix \emph{EDA-results.txt}.

Two remarkable findings came out of the analysis.

First, the test dataset is too small for the number of different relations. Some relations are so rare that there are only one or a few examples of them. 
As a result, they cannot be evaluated correctly.

Second, the use of `none' as a generation is too common. This refers to an empty tail. 3.08\% of all generations, 18.4\% of the first generations and 20.68\% of the greedy generations are `none'. The latter is a problem because this is the only value used for evaluation with Bleu in preprocess\_generations in automatic\_eval.py. The high percentage of the first generations is an even bigger problem, as it is used in the code's main evaluation function: topk\_eval. This function evaluates the top k examples using various metrics. It produces a table with a comparison of the results shown in the paper. However, this function is called with k=1, so that only the first generation is evaluated. Most generations other than `none' resemble the tail, but do not match exactly. Since `none' is likely to yield an exact match, the evaluation results are too optimistic.

\subsection{Test data generation}\label{testdatagen}
The Github page\cite{github-comet-atomic2020} for the code for the paper refers to the Atomic 2020 dataset\cite{atomic2020} used. This dataset is split into train, dev and test. The test data consist of 152,209 records with a head, a relation and a tail. This data was never used to train the model and thus can be used for evaluation. 

We used this data to create new generations with the pre-trained Comet-Atomic-2020-BART model in the project. To do this, we created a module \emph{test\_data\_generator.py}, in which the records are read, tokenized and converted into queries. These queries are sent to the model in batches of 32. The model then creates five generations for each query. The first generation that is not `none' is used as the greedy generation. This way the results are not skewed to `none'.

As a result, 34,689 records were generated. These are fewer than in the test dataset because generations with the same head and relation were merged, allowing existing evaluation code to be reused. The records are stored is the file \emph{test\_data\_generations.json}. This file will be used for the following experiments.

\subsection{Automatic evaluation of data splits}\label{autoeval}
The dataset can be split in many ways. To automate the splitting of the data and evaluating the splits, we created the module \emph{automatic\_eval\_data\_split.py}. This takes the data in our test data file and splits it using bucket functions, in the same way as in exploratory data analysis. Then each bucket is evaluated individually with only the greedy generations, similar to topk\_eval in the existing module automatic\_eval.py.

However, the top-k evaluation is simplified because only k=1 is used. The function topk\_eval is skipped and the evaluation function called in it is called directly with the greedy generation. This function evaluates generations using 8 different metrics: Bleu 1 to 4, METEOR, ROUGE-L, CIDEr, and BERT score. It provides a table of results similar to Table 7 in the paper\cite{Hwang2021COMETATOMIC2O}. Also, Bleu is no longer evaluated separately because it is already done here.

\section{Experiments and results}
Before starting to experiment with different splits of the data, we had to compare the evaluation of all generated test data with the data provided in the project. This will set a new baseline for the experiments. Table \ref{baseline_comparison} shows a comparison of the evaluation in the paper with the new baseline for the generated test data. These results are significantly worse, probably because the `none' results are no longer rewarded as much.

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
               & Bleu-1 & Bleu-2 & Bleu-3 & Bleu-4 & METEOR & ROUGE-L & CIDEr & BERT Score \\ \hline
paper & 0.462  & 0.280  & 0.182  & 0.124  & 0.325  & 0.486   & 0.632 & 0.636      \\ \hline
new baseline   & 0.365  & 0.222  & 0.152  & 0.110  & 0.245  & 0.274   & 0.315 & 0.552      \\ \hline
\end{tabular}
\caption{Comparison of evaluation in paper with new baseline \label{baseline_comparison}}
\end{table}

After establishing this new baseline, we have conducted several experiments with different data splits of the generated test data.

\subsection{Unary vs binary}

The data is split between unary relations that involve only PersonX and binary relations that also involve PersonY. The results are shown in table \ref{unary_binary_comparison}. Binary relations have slightly higher scores, but the differences are small.


\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
               & Bleu-1 & Bleu-2 & Bleu-3 & Bleu-4 & METEOR & ROUGE-L & CIDEr & BERT Score \\ \hline
unary & 0.354 & 0.219 & 0.152 & 0.111 & 0.237 & 0.263 & 0.306 & 0.544      \\ \hline
binary   & 0.397 & 0.230 & 0.153 & 0.107 & 0.266 & 0.305 & 0.329 & 0.5762      \\ \hline
\end{tabular}
\caption{Comparison of unary vs binary relations \label{unary_binary_comparison}}
\end{table}

\subsection{Mask vs no mask}
The data is split between heads that have a mask (\_\_\_) and heads that have no mask. The results are shown in table \ref{mask_comparison}. The heads with a mask score worse than the heads without a mask. However, for the BERT score, this difference is not as large as for the other metrics. This is probably because BERT is trained to fill masks, so it handles these gaps more easily.

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
               & Bleu-1 & Bleu-2 & Bleu-3 & Bleu-4 & METEOR & ROUGE-L & CIDEr & BERT Score \\ \hline
mask & 0.295 & 0.178 & 0.117 & 0.084 & 0.212 & 0.228 & 0.209 & 0.525      \\ \hline
no mask   & 0.376 & 0.228 & 0.157 & 0.113 & 0.251 & 0.283 & 0.334 & 0.558      \\ \hline
\end{tabular}
\caption{Comparison of mask vs no mask \label{mask_comparison}}
\end{table}

\subsection{Tail length}
The data is split depending on the average number of words in the tail. The counts from 1 to 6 form their own category. All tails with more then 7 words are merged into a single category, because their occurrences are rare. See table \ref{tail_count_comparison}. Surprisingly, the results of tails with 5 or more words are better than the baseline for all metrics. However, tails with only a single word are much worse than the baseline. This is undoubtedly because `none' is occurring so often in the test data. In our generated data, these are explicitly skipped. This made the score worse than it should be for single word tails, because many valid generations were skipped as well. However, this also sheds doubt on the validity of the model in general for tails of length 1. During exploratory data analysis, it was discovered that single word tails occurred in 30.96\% of the cases, and that 20.68\% of the greedy generations were `none'. This makes it likely that the single tail category is dominated by this word only. When a large model like BERT is trained on this, it will quickly learn to predict this word most of the time. Therefore, the model is not a good fit for this category.

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
               & Bleu-1 & Bleu-2 & Bleu-3 & Bleu-4 & METEOR & ROUGE-L & CIDEr & BERT Score \\ \hline
tail 1 & 0.097 & 0.018 & 0.000 & 0.000 & 0.111 & 0.134 & 0.138 & 0.542      \\ \hline
tail 2   & 0.232 & 0.127 & 0.074 & 0.044 & 0.201 & 0.210 & 0.240 & 0.508      \\ \hline
tail 3   & 0.386 & 0.237 & 0.159 & 0.111 & 0.321 & 0.329 & 0.331 & 0.515      \\ \hline
tail 4   & 0.449 & 0.278 & 0.193 & 0.141 & 0.357 & 0.378 & 0.397 & 0.545      \\ \hline 
tail 5   & 0.437 & 0.261 & 0.177 & 0.130 & 0.328 & 0.375 & 0.519 & 0.593      \\ \hline
tail 6   & 0.415 & 0.237 & 0.156 & 0.110 & 0.286 & 0.346 & 0.422 & 0.604      \\ \hline
tail 7   & 0.423 & 0.257 & 0.177 & 0.127 & 0.293 & 0.349 & 0.372 & 0.608      \\ \hline
tail \textgreater7   & 0.402 & 0.242 & 0.163 & 0.115 & 0.273 & 0.331 & 0.327 & 0.595      \\ \hline
\end{tabular}
\caption{Comparison of average tail counts \label{tail_count_comparison}}
\end{table}

\subsection{Relations}

Many relations only rarely appear in the data. Evaluating relations that occur so infrequently is difficult because the variance is too high depending on chance. Therefore, the relations are first grouped into common and rare relations and evaluated to test whether this is indeed problematic. However, as can be seen in table \ref{common_rare_comparison}, this is not the case as both categories have similar results.

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
               & Bleu-1 & Bleu-2 & Bleu-3 & Bleu-4 & METEOR & ROUGE-L & CIDEr & BERT Score \\ \hline
common & 0.365 & 0.220 & 0.150 & 0.109 & 0.245 & 0.277 & 0.322 & 0.555      \\ \hline
rare   & 0.373 & 0.253 & 0.187 & 0.131 & 0.243 & 0.247 & 0.224 & 0.532      \\ \hline
\end{tabular}
\caption{Comparison of common vs rare relations \label{common_rare_comparison}}
\end{table}

Since the distinction of common and rare relations is not useful, all relations are evaluated separately after all. The results are shown in table \ref{relation_comparison}. The Bleu scores drop to zero in many cases, while the BERT Score is still comparable to before. For example, the relation `Causes' has only zeroes in other metrics, but a decent 0.525 as the BERT Score. Therefore, it seems that this scoring mechanism is better suitable for the data than the others. This makes sense, since the trained model uses BART, which is derived from BERT. Most relations have a score between 0.45 and 0.6. Three relations score worse: oEffect, NotDesires and xReason. Five relations score higher: xAttr, xReact, AtLocation, isAfter and isBefore.

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
               & Bleu-1 & Bleu-2 & Bleu-3 & Bleu-4 & METEOR & ROUGE-L & CIDEr & BERT Score \\ \hline
oEffect & 0.066 & 0.029 & 0.015 & 0.007 & 0.062 & 0.060 & 0.052 & 0.380 \\ \hline
oReact & 0.140 & 0.065 & 0.042 & 0.034 & 0.075 & 0.096 & 0.121 & 0.573 \\ \hline
oWant & 0.290 & 0.168 & 0.116 & 0.083 & 0.224 & 0.253 & 0.206 & 0.462 \\ \hline
xAttr & 0.370 & 0.169 & 0.003 & 0.000 & 0.213 & 0.263 & 0.204 & 0.649 \\ \hline
xEffect & 0.208 & 0.107 & 0.063 & 0.037 & 0.177 & 0.178 & 0.135 & 0.455 \\ \hline
xIntent & 0.434 & 0.299 & 0.212 & 0.159 & 0.361 & 0.376 & 0.482 & 0.536 \\ \hline
xNeed & 0.468 & 0.302 & 0.217 & 0.169 & 0.385 & 0.394 & 0.399 & 0.503 \\ \hline
xReact & 0.270 & 0.086 & 0.045 & 0.000 & 0.153 & 0.210 & 0.264 & 0.629 \\ \hline
xWant & 0.504 & 0.316 & 0.216 & 0.156 & 0.437 & 0.433 & 0.343 & 0.530 \\ \hline
AtLocation & 0.218 & 0.169 & 0.003 & 0.000 & 0.168 & 0.172 & 0.405 & 0.608 \\ \hline
ObjectUse & 0.503 & 0.331 & 0.230 & 0.154 & 0.366 & 0.371 & 0.189 & 0.480 \\ \hline
Desires & 0.032 & 0.000 & 0.000 & 0.000 & 0.008 & 0.017 & 0.016 & 0.469 \\ \hline
HasProperty & 0.150 & 0.117 & 0.088 & 0.000 & 0.101 & 0.100 & 0.283 & 0.490 \\ \hline
NotDesires & 0.016 & 0.000 & 0.000 & 0.000 & 0.005 & 0.012 & 0.036 & 0.433 \\ \hline
Causes & 0.000 & 0.000 & 0.000 & 0.000 & 0.000 & 0.000 & 0.000 & 0.525 \\ \hline
HasSubEvent & 0.383 & 0.253 & 0.000 & 0.000 & 0.294 & 0.272 & 0.127 & 0.492 \\ \hline
xReason & 0.025 & 0.000 & 0.000 & 0.000 & 0.037 & 0.021 & 0.017 & 0.420 \\ \hline
CapableOf & 0.113 & 0.060 & 0.034 & 0.000 & 0.106 & 0.095 & 0.191 & 0.501 \\ \hline
MadeUpOf & 0.146 & 0.084 & 0.000 & 0.000 & 0.086 & 0.105 & 0.202 & 0.572 \\ \hline
isAfter & 0.355 & 0.183 & 0.121 & 0.085 & 0.251 & 0.327 & 0.546 & 0.662 \\ \hline
isBefore & 0.343 & 0.165 & 0.099 & 0.067 & 0.234 & 0.318 & 0.490 & 0.647 \\ \hline
isFilledBy & 0.269 & 0.130 & 0.003 & 0.000 & 0.220 & 0.228 & 0.149 & 0.584 \\ \hline
HinderedBy & 0.524 & 0.343 & 0.241 & 0.174 & 0.351 & 0.382 & 0.243 & 0.587 \\ \hline
\end{tabular}
\caption{Comparison of relations \label{relation_comparison}}
\end{table}


\section{Created files}

A number of extra modules have been added to the code that accompanies the paper. They are listed in Table \ref{created_files}. The relative path points to the directory relative to the root of the code.

The appendices for this are listed in Table \ref{appendices}.

\begin{table}[H]
\begin{tabular}{|l|l|l|}
\hline
\textbf{Filename}               & \textbf{Relative path}           & \textbf{Described in section} \\ \hline
exploratory\_data\_analysis.py  & /system\_eval/                   &   \nameref{EDA}                   \\ \hline
automatic\_eval\_data\_split.py & /system\_eval/                   &   \nameref{autoeval}                   \\ \hline
test\_data\_generator.py        & /models/comet\_atomic2020\_bart/ & \nameref{testdatagen}                     \\ \hline
\end{tabular}
\caption{Created Python files in project \label{created_files}}
\end{table}


\begin{table}[]
\begin{tabular}{|l|l|l|}
\hline
\textbf{Filename}               & \textbf{Description} \\ \hline
EDA-results.txt  & the results of exploratory data analysis                      \\ \hline
test\_data\_generations.json  & the generated test data                      \\ \hline
code-ktai41.zip & the code project including the added modules as a zip-file                      \\ \hline
\end{tabular}
\caption{Appendices \label{appendices}}
\end{table}


\section{Conclusion}

This research provides a global picture of the possibilities of CSKGs. The field of commonsense reasoning is very large, so it is difficult not to exceed the time limit and get lost in details. Despite this, we managed to successfully conduct some useful experiments.

Most of the experiments have produced similar results to the paper. However, one significant error was discovered. The word `none' occurs too often in the data. The evaluation code does not correct this but makes it worse by only evaluating the greedy or the first generations, which are much more likely to be `none' than the other generations. This makes it likely that the model is biased to produce this tail.

The evaluation results also differ substantially depending on which metric is used. The BERT score is more consistent and also higher than any other metric. This is probably because the model uses BART, which is derived from BERT.

A better analysis could be made if there were more data and if the `none' tails ratio were smaller.


\bibliographystyle{unsrt} % We choose the "plain" reference style
\bibliography{report} % Entries are in the report.bib file

\end{document}
