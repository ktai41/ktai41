import argparse
from collections import defaultdict
from exploratory_data_analysis import load_data, create_buckets, relation_bucket_function, unary_binary_bucket_function, \
    mask_bucket_function, tail_count_bucket_function
from evaluation.eval import QGEvalCap
from tabulate import tabulate
import os
import logging

# Huggingface Bert-models always outputs the same warning, polluting the test logs, so it is turned off
logging.disable(logging.INFO)
logging.disable(logging.WARNING)

TEST_DATA_FILE = '../models/comet_atomic2020_bart/test_data_generations.json'


def eval_greedy_generations(model_name, data):
    """
    Evaluates greedy generations of the data
    """
    all_tails = {}
    greedy_gens = {}
    for i, data_row in enumerate(data):
        tails = data_row["tails"]
        gen = data_row["greedy"]
        key = str(i) + "_0"
        all_tails[key] = tails
        greedy_gens[key] = [gen]
    qg_eval = QGEvalCap(model_name, all_tails, greedy_gens)
    score, scores = qg_eval.evaluate()
    return score, scores


def split_and_evaluate_data(data, split_function):
    """
    The data is split into multiple buckets using the split-function and then the buckets are evaluated
    """
    buckets = create_buckets(data, split_function)
    return evaluate_buckets(buckets)


def evaluate_buckets(buckets):
    """
    Evaluates buckets of data (data-splits)
    """
    rows = []
    columns = []
    for bucket_key, bucket_data in buckets.items():
        scores, _ = eval_greedy_generations(bucket_key, bucket_data)
        if len(columns) == 0:
            columns = list(scores.keys())
            rows.append([""] + columns)  # header-row
        rows.append(to_row(bucket_key, scores, columns))
    return rows


def evaluate_all(data):
    """
    All data is evaluated
    """
    print('Evaluation of all data')
    scores = split_and_evaluate_data(data, lambda _: 'baseline')
    print(tabulate(scores, tablefmt='tsv', floatfmt='#.3f'))
    return scores


def split_and_evaluate_relations(data):
    """
    The data is split by relation and then evaluated
    """
    print('Evaluation of data split by relation')
    scores = split_and_evaluate_data(data, relation_bucket_function)
    print(tabulate(scores, tablefmt='tsv', floatfmt='#.3f'))
    return scores


def split_and_evaluate_unary_binary(data):
    """
    The data is split in unary and binary relations and then evaluated
    """
    print('Evaluation of unary vs binary relations')
    scores = split_and_evaluate_data(data, unary_binary_bucket_function)
    print(tabulate(scores, tablefmt='tsv', floatfmt='#.3f'))
    return scores


def split_and_evaluate_masks_or_not(data):
    """
    The data is split in heads with and without masks and then evaluated
    """
    print('Evaluation of masks vs no masks')
    scores = split_and_evaluate_data(data, mask_bucket_function)
    print(tabulate(scores, tablefmt='tsv', floatfmt='#.3f'))
    return scores


def split_and_evaluate_avg_tail_count(data):
    """
    The data is split  by average tail count and then evaluated
    """
    print('Evaluation of average tail counts')
    scores = split_and_evaluate_data(data, tail_count_bucket_function)
    print(tabulate(scores, tablefmt='tsv', floatfmt='#.3f'))
    return scores


def split_and_evaluate_common_and_rare_relations(data):
    """
    The data is split in common and rare relations and then evaluated
    """
    print('Evaluation of common vs rare relations')
    total_count = len(data)
    rare_threshold = 5.0
    bucket_dict = create_buckets(data, relation_bucket_function)
    rare_bucket_dict = defaultdict(list)
    for bucket_key, bucket_data in bucket_dict.items():
        percentage = round(len(bucket_data) / total_count * 100, 2)
        if percentage < rare_threshold:
            rare_bucket_dict['rare'].extend(bucket_data)
        else:
            rare_bucket_dict['common'].extend(bucket_data)
    scores = evaluate_buckets(rare_bucket_dict)
    print(tabulate(scores, tablefmt='tsv', floatfmt='#.3f'))
    return scores


def to_row(name, results, columns):
    """
    Creates a row with a name columns and more floating point columns with results
    """
    return [name] + [format(float(results[c]), '#.3f') for c in columns]


def main(input_file):
    """
    Splits and evaluated an input file in multiple ways
    """
    data = load_data(input_file)
    if len(data) == 0:
        print('Input file is empty')
        return

    model_name = os.path.basename(input_file).split('.')[0]
    print('Evaluation of ' + model_name)

    evaluate_all(data)
    split_and_evaluate_unary_binary(data)
    split_and_evaluate_masks_or_not(data)
    split_and_evaluate_avg_tail_count(data)
    split_and_evaluate_common_and_rare_relations(data)
    split_and_evaluate_relations(data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_file', type=str, help='Results file on ATOMIC2020 test set')
    args = parser.parse_args()
    if args.input_file:
        main(args.input_file)
    else:
        main(TEST_DATA_FILE)
