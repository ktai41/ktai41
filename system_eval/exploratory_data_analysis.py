import json
from collections import defaultdict
from nltk import word_tokenize

DATA_FILE = 'BART/BART-atomic_2020.json'


def relation_bucket_function(data_row):
    """
    Bucket function that bases the bucket key on the relation
    """
    return data_row['relation']


def unary_binary_bucket_function(data_row):
    """
    Bucket function that bases the bucket key on whether the head contains a single PersonX or also a PersonY
    """
    if 'PersonY' in data_row['head']:
        return 'binary'
    else:
        return 'unary'


def mask_bucket_function(data_row):
    """
    Bucket function that bases the bucket key on whether the head contains a mask (___)
    """
    if '___' in data_row['head']:
        return 'mask'
    else:
        return 'no_mask'


def tail_count_bucket_function(data_row):
    """
    Bucket function that bases the bucket key on the average number of words in the tail
    """
    tail_lengths = [len(word_tokenize(tail)) for tail in data_row['tails']]
    avg_tail = int(round((sum(tail_lengths) / len(tail_lengths)), 0))
    if avg_tail > 7:
        return 'avg tail >7'
    else:
        return f'avg tail {avg_tail}'


def create_buckets(data, bucket_function):
    """
    Splits the data into multiple buckets using the bucket function. The buckets are a dictionary with a
    bucket-description as key and data-rows as value.
    """
    result = defaultdict(list)
    for data_row in data:
        bucket_key = bucket_function(data_row)
        result[bucket_key].append(data_row)
    return result


def print_buckets(bucket_dict: dict, total_count: int):
    """
    Prints the buckets keys, count and percentage
    """
    for bucket_key, bucket_data in bucket_dict.items():
        percentage = round(len(bucket_data) / total_count * 100, 2)
        print(f'Bucket {bucket_key}: {len(bucket_data)} ({percentage}%)')


def analyze_data(input_file, bucket_function):
    """
    Analyzes the data in the input file by splitting it into multiple buckets using the bucket function
    """
    data = load_data(input_file)
    bucket_dict = create_buckets(data, bucket_function)
    print_buckets(bucket_dict, len(data))


def analyze_rare_relations(input_file):
    """
    Analyzes the data in the input file by splitting it into buckets for each relation. Then all buckets with less then
    5% of the data are combined into a 'rare' bucket and the others into a common bucket
    """
    data = load_data(input_file)
    total_count = len(data)
    rare_threshold = 5.0
    bucket_dict = create_buckets(data, relation_bucket_function)
    rare_bucket_dict = defaultdict(list)
    for bucket_key, bucket_data in bucket_dict.items():
        percentage = round(len(bucket_data) / total_count * 100, 2)
        if percentage < rare_threshold:
            rare_bucket_dict['rare'].extend(bucket_data)
        else:
            rare_bucket_dict['common'].extend(bucket_data)
    print_buckets(rare_bucket_dict, total_count)


def analyze_none_generations(input_file):
    data = load_data(input_file)
    gen_count = 0
    none_count = 0
    greed_none_count = 0
    first_none_generations = 0
    for data_row in data:
        is_first = True
        for gen in data_row['generations']:
            gen_count += 1
            if gen.strip() == 'none':
                none_count += 1
                if is_first:
                    first_none_generations += 1
            is_first = False
        if data_row['greedy'].strip() == 'none':
            greed_none_count += 1
    percentage = round(none_count / gen_count * 100, 2)
    print(f'None-generations: {none_count} of {gen_count} ({percentage}%)')
    percentage = round(greed_none_count / len(data) * 100, 2)
    print(f'Greedy none-generations: {greed_none_count} of {len(data)} ({percentage}%)')
    percentage = round(first_none_generations / len(data) * 100, 2)
    print(f'First none-generations: {first_none_generations} of {len(data)} ({percentage}%)')


def load_data(input_file):
    """
    Loads the data in the input file and returns it as json rows
    """
    result = []
    with open(input_file) as file:
        for line in file:
            result.append(json.loads(line))
    return result


def save_data(output_file, data):
    with open(output_file, 'w') as file:
        file.writelines([json.dumps(x) for x in data])


def main():
    print('Analyze relations')
    analyze_data(DATA_FILE, relation_bucket_function)

    print('\nAnalyze unary vs binary')
    analyze_data(DATA_FILE, unary_binary_bucket_function)

    print('\nAnalyze masks vs no masks')
    analyze_data(DATA_FILE, mask_bucket_function)

    print('\nAnalyze rare vs non-rare relations')
    analyze_rare_relations(DATA_FILE)

    print('\nAnalyze tail counts')
    analyze_data(DATA_FILE, tail_count_bucket_function)

    print('\nAnalyze none generations')
    analyze_none_generations(DATA_FILE)


if __name__ == "__main__":
    main()
