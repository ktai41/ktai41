import json
import math
from collections import defaultdict
from tqdm import tqdm
from nltk import word_tokenize
from generation_example import Comet, all_relations


TEST_DATA_FILE = 'test_data_atomic2020.tsv'
OUTPUT_FILE = 'test_data_generations.json'
ALL_RELATIONS = set(all_relations)


class Atomic2020Record:
    """
    Class to contain head, relation, tail and generations for a single record of Atomic2020 data
    """
    def __init__(self, line):
        self.line = line
        head, relation, tail = split_head_relation_tail(line)
        self.head = head
        self.relation = relation
        self.tails = [tail]
        self.query = "{} {} [GEN]".format(head, relation)
        self.generations = []

    def has_relation(self) -> bool:
        """
        Returns whether the record has a relation
        """
        return len(self.relation) > 0

    def get_greedy_generation(self):
        """
        Gets the greedy generation (usually first generations, although 'none' is skipped)
        """
        for gen in self.generations:
            # 'none' is skipped because it occurs to frequently, so that the model is rewarded for outputting nothing.
            # Otherwise, the first generation is used. It is unknown how else to estimate the best result.
            # Usually, generations are sorted from best to worst, in which case this would work, but it is unknown
            # if this is also the case here.
            if gen.strip() != 'none':
                return gen
        return 'none'

    def to_json(self):
        """
        Generates a json that can be used for saving and evaluating
        """
        return json.dumps({"head": self.head, "relation": self.relation, "tails": self.tails,
                           "generations": self.generations, "greedy": self.get_greedy_generation()})


def read_atomic2020_records(input_file):
    """
    Reads the records in the input file as Atomic2020Record objects.
    """
    records = []
    with open(input_file, 'r') as f:
        for line in f.readlines():
            record = Atomic2020Record(line)
            if record.has_relation():
                records.append(record)
    return records


def merge_records_with_same_head_and_relation(records: list[Atomic2020Record]) -> list[Atomic2020Record]:
    """
    Records with the same head and relation are merge into a single record with multiple tails.
    """
    records_per_head_relation = defaultdict(list[Atomic2020Record])
    result = []
    for record in records:
        key = record.head + record.relation
        records_per_head_relation[key].append(record)
    for records in records_per_head_relation.values():
        tails = []
        for record in records:
            tails.extend(record.tails)
        # first record is reused as a merger
        merged_record = records[0]
        merged_record.tails = tails
        result.append(merged_record)
    return result


def split_head_relation_tail(line: str) -> tuple[str, str, str]:
    """
    Splits a line from atomic2020 data into head, relation and tail
    The line can be like: PersonX gains confidence	xAttr	prideful
    In this case the head is 'PersonX gains confidence', the relation is 'xAttr' and the tail is 'prideful'.
    """
    words = word_tokenize(line)
    relations = set(words).intersection(ALL_RELATIONS)
    if len(relations) != 1:
        return line, '', ''  # skip splitting line since either no or multiple relations are found
    relation = relations.pop()
    line_parts = line.split(relation)
    head = line_parts[0].strip()
    tail = line_parts[1].strip()
    return head, relation, tail


def add_generations_to_records(records: list[Atomic2020Record]):
    """
    Adds generations to Atomic2020 records by calling Comet on each query of each record
    """
    comet = Comet("./comet-atomic_2020_BART")
    batch_size = 32
    comet.batch_size = batch_size
    comet.model.zero_grad()
    queries = [x.query for x in records]
    batch_count = math.ceil(len(queries) / batch_size)
    for batch in tqdm(range(batch_count)):
        start_idx = batch * batch_size
        end_idx = min(start_idx + batch_size, len(queries))
        batch_queries = queries[start_idx: end_idx]
        batch_records = records[start_idx: end_idx]
        batch_generations = comet.generate(batch_queries, decode_method="beam", num_generate=5)
        # result is a list with a single nested list with batch_size * num_generate elements
        batch_generations = batch_generations[0]  # get nested list
        # loop through 1 record and 5 generations at the same time
        record_idx = 0
        for idx in range(0, len(batch_generations), 5):
            record = batch_records[record_idx]
            record.generations = batch_generations[idx:idx+5]
            record_idx += 1
        continue


def generate_test_data():
    """
    Creates generations for all records in TEST_DATA_FILE and saves the results to OUTPUT_FILE.
    """
    records = read_atomic2020_records(TEST_DATA_FILE)
    records = merge_records_with_same_head_and_relation(records)
    add_generations_to_records(records)
    jsons = [x.to_json() + '\n' for x in records]
    with open(OUTPUT_FILE, 'w') as f:
        f.writelines(jsons)


if __name__ == "__main__":
    generate_test_data()
